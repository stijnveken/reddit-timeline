import requests
import os
import time
import re
import optparse

REDDIT_URL_REGEX = "(http:\/\/|https:\/\/){1}(www\.reddit\.com)+\/r\/+\w*\/comments\/\w{6}\/+\w*\/\w*\/*"
REDDIT_COMMENT_PERMALINK_REGEX = "(http:\/\/|https:\/\/){1}(www\.reddit\.com)+\/r\/+\w*\/comments\/\w{6}\/+\w*\/\w*\/"


def read_file(file_path):
    if os.path.isabs(file_path):
        url_file = open(file_path, mode='r')
        return url_file
    else:
        current_directory = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(current_directory, file_path)
        url_file = open(file_path, mode='r')
        return url_file


def get_table_data(urls):
    data = []
    print("Fetching timeline data...")
    for url in urls:
        if re.match(REDDIT_COMMENT_PERMALINK_REGEX, url):
            url_json = requests.get(url=url+".json", headers={'User-agent': 'reddit timeline bot 0.1'}).json()
            post_data = {
                "kind": "comment",
                "text": url_json[1]["data"]["children"][0]["data"]["body"],
                "author": url_json[1]["data"]["children"][0]["data"]["author"],
                "created": url_json[1]["data"]["children"][0]["data"]["created_utc"],
                "url": url
            }
            data.append(post_data)
        else:
            url_json = requests.get(url=url+".json", headers={'User-agent': 'reddit timeline bot 0.1'}).json()
            post_data = {
                "kind": "post",
                "author": url_json[0]["data"]["children"][0]["data"]["author"],
                "created": url_json[0]["data"]["children"][0]["data"]["created_utc"],
                "url": "https://redd.it/" + url_json[0]["data"]["children"][0]["data"]["id"],
                "submission_url": url_json[0]["data"]["children"][0]["data"]["url"],
                "title": url_json[0]["data"]["children"][0]["data"]["title"]
            }
            data.append(post_data)

        print(str(urls.index(url) + 1) + "/" + str(len(urls)))
        if urls.index(url) < len(urls):
            time.sleep(1)
    print("Fetched and sorted timeline data...")
    return sorted(data, key=lambda k: k["created"])


def create_table(urls):
    print("Starting timeline...")
    timeline_file = open("timeline.txt", mode='w')
    timeline = ""
    timeline += create_table_head()

    data = get_table_data(urls)

    for d in data:
        timeline += create_table_row(
            url=d.get("url"),
            submission_url=d.get("submission_url"),
            author=d.get("author"),
            text=d.get("text"),
            title=d.get("title"),
            kind=d.get("kind")
        )

    timeline_file.write(timeline)
    timeline_file.close()
    print("Done")


def create_table_head():
    return "User | Post | Thread\n---|---|---\n"


def create_table_row(url, submission_url, author, text, title, kind):
    TABLE_ROW_TEXT = "/u/{} | {} | [Thread]({})\n"
    TABLE_ROW_URL = "/u/{} | [{}]({}) | [Thread]({})\n"

    if kind == "post":
        return TABLE_ROW_URL.format(author, title, submission_url, url)
    else:
        return TABLE_ROW_TEXT.format(author, text, url)


def parse_urls(url_file):
    parsed_urls = []
    urls = url_file.readlines()
    urls = [url.strip('\n') for url in urls]  # Strip \n from urls

    for url in urls:
        if re.match(REDDIT_URL_REGEX, url) or re.match(REDDIT_COMMENT_PERMALINK_REGEX, url):
            parsed_urls.append(url)
        else:
            print("URL rejected: {}".format(url))

    print("Parsed urls...")
    return parsed_urls


def main():
    parser = optparse.OptionParser()
    parser.add_option("-f", "--file",
                      dest="file",
                      help="write timeline to file")

    options, args = parser.parse_args()
    options = vars(options)

    file_path = options["file"]
    url_file = read_file(file_path)
    parsed_urls = parse_urls(url_file)
    print("File loaded...")
    create_table(parsed_urls)


if __name__ == "__main__":
    main()
